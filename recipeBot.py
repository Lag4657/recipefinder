import requests
import json
import discord
from discord.ext import commands
from discord import app_commands, Intents
import datetime
import time
import sys
from discord.ui import Button 
from typing import *
from discord.app_commands import Choice

fConfig = open('config.json',)
config = json.load(fConfig)['info']
serverID = 1188489620195901550
bot = commands.Bot(command_prefix='!',intents=Intents.all())


class RecipeView(discord.ui.View):
    def __init__(self,parentView):
        super().__init__(timeout=None)
        self.parentView = parentView

    @discord.ui.button(label='Back',style=discord.ButtonStyle.red)
    async def button1(self,interaction:discord.Interaction,button:discord.ui.Button):
        await interaction.response.defer()
        await interaction.edit_original_response(content='Which recipe would you like to see?',view=self.parentView,embed=None)


class RecipeListView(discord.ui.View):
    def __init__(self,recipes):
        super().__init__(timeout=None)
        for recipe in recipes:
            self.add_item(RecipeButton(recipe,self))
    
class RecipeButton(discord.ui.Button):
    def __init__(self,recipe,view):
        super().__init__(style=discord.ButtonStyle.blurple, label=recipe['title'])
        self.parentView = view
        self.recipe = recipe
    
    async def callback(self, interaction: discord.Interaction):
        await interaction.response.defer()
        embeds = recipeDetails(self.recipe)
        await interaction.edit_original_response(content='Here is the recipe:',embeds=embeds,view=RecipeView(self.parentView))


class ButtonList(discord.ui.View):
    def __init__(self,list,finalLabel:str=None,finalColor:discord.ButtonStyle=None,listColor=discord.ButtonStyle.blurple):
        super().__init__(timeout=None)
       
        for label in list:
            
            self.add_item(Button(label=label,style=listColor))
        if finalLabel != None:
            self.add_item(Button(label=finalLabel,style=finalColor))

async def getButton(view:discord.ui.View) -> (discord.Interaction, discord.Button):
    while True:
        try:
            res = await bot.wait_for('interaction', check=lambda interaction: interaction.data["component_type"] == 2 and "custom_id" in interaction.data.keys())
            break
        except:
            pass
            
    for item in view.children:
        if item.custom_id == res.data["custom_id"]:
            print(res,' ',item)
            return res, item
        
def recipeDetails(recipe:dict)->discord.Embed:
    embed=discord.Embed(title=recipe['title'], color=0x00ff00)
    join = '\n'
    ingredientList = set()
    for ingredient in recipe['extendedIngredients']:
        ingredientList.add(f"{ingredient['nameClean']} {ingredient['measures']['metric']['amount']} {ingredient['measures']['metric']['unitShort']}")

    ingredients = join.join(ingredientList)

    embed.add_field(name='Ingredients',value=ingredients,inline=True)
    
    equipmentSet = set()
    instructionList = list()

    for step in recipe['analyzedInstructions'][0]['steps']:
        for item in step['equipment']:
            equipmentSet.add(item['name'])
        instructionList.append(f"{step['number']}. {step['step']}")

      
    equipment = join.join(equipmentSet)

    embed.add_field(name='Equipment',value=equipment,inline=True)
    embed2 = discord.Embed(title='Instructions', color=0x00ff00)
    in1,in2 = list(),list()
    i = 0
    for instruction in instructionList:
        if i < (len(instructionList)/2):
            in1.append(instruction)
        else:
            in2.append(instruction)
        i +=1 
    in1 = join.join(in1)
    in2 = join.join(in2) 
    embed2.add_field(name='',value=in1,inline=True)
    embed2.add_field(name='',value=in2,inline=True)
    return [embed,embed2]



@bot.event
async def on_ready():
    try:
        synced = await bot.tree.sync(guild=discord.Object(id=serverID))
        print('Bot is ready')
    except Exception as e:
        print(e)

@bot.tree.command(name='random-recipe',guild=discord.Object(id=serverID),description='Get 5 random recipes')
@app_commands.describe(meal='What kind of recipes are you looking for?',vegetarian='Would you like them to be vegetarian?')
@app_commands.choices(meal=[
        Choice(name='dinner', value=1),
        Choice(name='dessert', value=2),
        Choice(name='lunch',value=3),
        Choice(name='salad',value=4),],
        vegetarian=[
            Choice(name='Yes',value=True),
            Choice(name='No',value=False)])
async def randomRecipe(interaction:discord.Interaction,meal:Choice[int],vegetarian:Choice[int]):
    
    if vegetarian.value:
        tags = f"{meal.name},vegetarian"
    else:
        tags = meal.name
    recipes = json.loads(requests.get(f'https://api.spoonacular.com/recipes/random?apiKey={config["API_KEY"]}&tags={tags}&number=5').content)['recipes']
    
    
    if len(recipes) == 0:
        await interaction.response.send_message(f'Sorry, we dont have any recipes matching those filters')
        return
    recipeView = RecipeListView(recipes)
    await interaction.response.send_message('What sounds most interesting?',view=recipeView)


@bot.tree.command(name='recipe-cuisine',guild=discord.Object(id=serverID),description='Get 5 recipes from a specific cuisine')
@app_commands.describe(cuisine='What cuisine would you like recipes from?',meal='What kind of recipes would you like?',vegetarian='Would you like it to be vegetarian?')
@app_commands.choices(meal=[
        Choice(name='main course', value=1),
        Choice(name='dessert', value=2),
        Choice(name='salad',value=3),
        Choice(name='snack',value=4)
        ],
        cuisine=[
            Choice(name='African',value=1),
            Choice(name='Asian',value=2),
            Choice(name='American',value=3),
            Choice(name='Cajun',value=4),
            Choice(name='Caribbean',value=5),
            Choice(name='Chinese',value=6),
            Choice(name='Eastern European',value=7),
            Choice(name='European',value=8),
            Choice(name='French',value=9),
            Choice(name='German',value=10),
            Choice(name='Greek',value=11),
            Choice(name='Indian',value=12),
            Choice(name='Irish',value=13),
            Choice(name='Italian',value=14),
            Choice(name='Japanese',value=15),
            Choice(name='Jewish',value=16),
            Choice(name='Korean',value=17),
            Choice(name='Latin American',value=18),
            Choice(name='Mediterranean',value=19),
            Choice(name='Mexican',value=20),
            Choice(name='Middle Eastern',value=21),
            Choice(name='Nordic',value=22),
            Choice(name='Spanish',value=23),
            Choice(name='Thai',value=24),
            Choice(name='Vietnamese',value=25)],
            vegetarian=[
            Choice(name='Yes',value=True),
            Choice(name='No',value=False)])
async def recipeByCuisine(interaction:discord.Interaction,cuisine:Choice[int],meal:Choice[int],vegetarian:Choice[int]):

    if vegetarian.value:
        veg = '&diet=vegetarian'
    else:
        veg = ''
    
    recipes = json.loads(requests.get(f'https://api.spoonacular.com/recipes/complexSearch?cuisine={cuisine.name}&type={meal.name}{veg}&sort=random&number=5&apiKey={config["API_KEY"]}').content)['results']
    if len(recipes) == 0:
        await interaction.response.send_message(f'Sorry, we dont have any recipes matching those filters')
        return
    recipeList = list()
    for recipe in recipes:
        recipeList.append(json.loads(requests.get(f'https://api.spoonacular.com/recipes/{recipe["id"]}/information?apiKey={config["API_KEY"]}').content))
    
    recipeView = RecipeListView(recipeList)
    await interaction.response.send_message('What sounds most interesting?',view=recipeView)
    

bot.run(config['BOT_TOKEN'])








